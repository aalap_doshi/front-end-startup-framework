# README #

This is a front-end responsive framework combining many elements from various other frameworks to the ones written here that establish a basic style guide for projects that come out of the Michigan Institute for Clinical & Health Research (MICHR).

### How do I get set up? ###

* Download the folder and use it as a start-up framework for your projects. Add your application specific styles to it if you need to. Get writing HTML and CSS quickly. No set up required other than integrating the markup into your application.

### Who do I talk to? ###

* Repo owner

### Acknowledgements ###

* Components used from Twitter Bootstrap, Zurb Foundation, Pure CSS, Typlate and Prism CSS among others.  